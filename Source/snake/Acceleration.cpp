// Fill out your copyright notice in the Description page of Project Settings.


#include "Acceleration.h"
#include "SnakeBase.h"

AAcceleration::AAcceleration()
{
	AccelerationSpeed  = 0.75;
}

void AAcceleration::Interact(AActor* Interactor, const bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
			Snake->SetMovementSpeed(Snake->GetMovementSpeed() * AccelerationSpeed);
		AFood::Interact(Interactor, bIsHead);
	}
}
