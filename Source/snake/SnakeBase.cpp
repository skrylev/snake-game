// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"

#include "Components/MeshComponent.h"
#include "SnakeElementBase.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.0f;
	MovementSpeed = 10.f;
	MaxSpeed = 20.f;
	MinSpeed = 0.1f;
	LastMoveDirection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(3);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation = FVector(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform = FTransform(GetActorLocation() - NewLocation);
		auto SnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		int32 ElemIndex = SnakeElements.Add(SnakeElem);
		SnakeElem->SnakeOwner = this;
		if (ElemIndex == 0)
		{
			SnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	float MovementSpeedDelta = ElementSize;
	
	switch (NextMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeedDelta;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeedDelta;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= MovementSpeedDelta;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += MovementSpeedDelta;
		break;
	}
	LastMoveDirection = NextMoveDirection;
	SnakeElements[0]->TogleCollision();
	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		if (SnakeElements[i]->IsHidden())
			SnakeElements[i]->SetActorHiddenInGame(false);

		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	if (SnakeElements[0]->IsHidden())
		SnakeElements[0]->SetActorHiddenInGame(false);

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->TogleCollision();

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (!IsValid(OverlappedElement))
		return;

	int32 ElemIndex;
	SnakeElements.Find(OverlappedElement, ElemIndex);
	const bool bIsFirst = ElemIndex == 0;
	IInteractable* Interface = Cast<IInteractable>(Other);
	
	if (!Interface)
		return;
	
	Interface->Interact(this, bIsFirst);
}

void ASnakeBase::GameOver()
{
	for (auto Elem : SnakeElements)
		Elem->Destroy();
	this->Destroy();
}

void ASnakeBase::SetMovementSpeed(float NewSpeed)
{

	if (NewSpeed <= MaxSpeed && NewSpeed >= MinSpeed) {

		MovementSpeed = NewSpeed;
		SetActorTickInterval(MovementSpeed);
	}
}

void ASnakeBase::TeleportSnake(const FVector TeleportLocation, const EMovementDirection NewMoveDirection)
{
	UE_LOG(LogTemp, Warning, TEXT("ASnakeBase::TeleportSnake %s"), *TeleportLocation.ToString());

	if (LastMoveDirection == NewMoveDirection)
		LastMoveDirection = EMovementDirection::NONE;
	NextMoveDirection = NewMoveDirection;
	// SnakeElements[0]->AddActorWorldOffset(SnakeElements[0]->GetActorLocation() - TeleportLocation);
	SnakeElements[0]->SetActorLocation(TeleportLocation);
}



