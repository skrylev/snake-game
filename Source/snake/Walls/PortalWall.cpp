// Fill out your copyright notice in the Description page of Project Settings.


#include "PortalWall.h"
#include "snake/SnakeBase.h"
#include "snake/SnakeElementBase.h"



APortalWall::APortalWall() : ExitPortal(nullptr)
{
}

void APortalWall::Interact(AActor* Interactor, const bool bIsHead)
{
    if (!IsValid(ExitPortal) || this == ExitPortal)
    {
        UE_LOG(LogTemp, Warning, TEXT("not Valid(ExitPortal) or exit == this"));

        AWall::Interact(Interactor, bIsHead);
        return;
    }
    ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
    FVector ExitLocation = ExitPortal->GetActorLocation();
    FVector SelfOrigin, ExitOrigin, OriginSnake, ExitBounds, SnakeHeadBounds, SelfBounds;
    ExitPortal->GetActorBounds(true, ExitOrigin, ExitBounds);
    Snake->SnakeElements[0]->GetActorBounds(true, OriginSnake, SnakeHeadBounds);
    GetActorBounds(true, SelfOrigin, SelfBounds);

    /* от -1 - левая/верхняя часть портала до 1 - правая/нижняя */
    float PortalRatioLocationX = (OriginSnake.X - SelfOrigin.X) / SelfBounds.X; 
    float PortalRatioLocationY = (OriginSnake.Y - SelfOrigin.Y) / SelfBounds.Y;
    //делается отступ относительно выходного портала относительно оси старого
    if (ExitPortal->ExitDirection == EMovementDirection::UP)
    {
        ExitLocation.X += ExitBounds.X + SnakeHeadBounds.X * 1.5;
        (this->ExitDirection == EMovementDirection::LEFT || this->ExitDirection == EMovementDirection::RIGHT)
            ? ExitLocation.Y += ExitBounds.Y * PortalRatioLocationX
            : ExitLocation.Y += ExitBounds.Y * PortalRatioLocationY;
    }
    else if (ExitPortal->ExitDirection == EMovementDirection::DOWN)
    {
        ExitLocation.X -= (ExitBounds.X + SnakeHeadBounds.X * 1.5);
        (this->ExitDirection == EMovementDirection::LEFT || this->ExitDirection == EMovementDirection::RIGHT)
            ? ExitLocation.Y += ExitBounds.Y * PortalRatioLocationX
            : ExitLocation.Y += ExitBounds.Y * PortalRatioLocationY;
    }
    else if (ExitPortal->ExitDirection == EMovementDirection::LEFT)
    {
        ExitLocation.Y -= (ExitBounds.Y + SnakeHeadBounds.Y * 1.5);
        (this->ExitDirection == EMovementDirection::LEFT || this->ExitDirection == EMovementDirection::RIGHT)
            ? ExitLocation.X += ExitBounds.X * PortalRatioLocationX
            : ExitLocation.X += ExitBounds.X * PortalRatioLocationY;
    }
    else if (ExitPortal->ExitDirection == EMovementDirection::RIGHT)
    {
        ExitLocation.Y += ExitBounds.Y + SnakeHeadBounds.Y * 1.5;
        (this->ExitDirection == EMovementDirection::LEFT || this->ExitDirection == EMovementDirection::RIGHT)
            ? ExitLocation.X += ExitBounds.X * PortalRatioLocationX
            : ExitLocation.X += ExitBounds.X * PortalRatioLocationY;        
    }
    Snake->TeleportSnake(ExitLocation, ExitPortal->ExitDirection);
}
