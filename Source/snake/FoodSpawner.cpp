#include "FoodSpawner.h"
// Fill out your copyright notice in the Description page of Project Settings.

#include "Food.h"
#include "Math/UnrealMathUtility.h" 
#include "Acceleration.h"
#include "Engine/World.h"


// Sets default values
AFoodSpawner::AFoodSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSpawner::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("BeginPlay") );

	for (int i = 0; i < 3; ++i)
		SpawnFood();

}

// Called every frame
void AFoodSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodSpawner::SpawnFood()
{
	if (LeftUpPoint.X < RightDownPoint.X || LeftUpPoint.Y > RightDownPoint.Y)
	{
		UE_LOG(LogTemp, Warning, TEXT("Bad min and max points of map") );
		return;
	}
	
	const FVector SpawnLocation = FVector(FMath::RandRange(RightDownPoint.X/10, LeftUpPoint.X/10),
		FMath::RandRange(LeftUpPoint.Y/10, RightDownPoint.Y/10),
		LeftUpPoint.Z);
	TSubclassOf<AFood> FoodActor;
	const int RandomNumber = FMath::RandRange(0, SpawnableFood.Num() - 1);
	const FTransform NewTransform = FTransform((SpawnLocation * 10));

	const auto& NewFood = GetWorld()->SpawnActor<AFood>(SpawnableFood[RandomNumber], NewTransform);
	if (IsValid(NewFood))
	{
		NewFood->FoodDestroyed.AddDynamic(this, &AFoodSpawner::SpawnFood);
		UE_LOG(LogTemp, Warning, TEXT("Food Location %s"), *SpawnLocation.ToString());
	}
}

TSubclassOf<AFood> AFoodSpawner::BakeFood()
{
	TSubclassOf<AAcceleration> Food;
	return Food;
}
